﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    int vertical;
    int horizontal;
    List<Placement> allPlacements;
    public Placement selectedPlacement;
    public PlayerTracker playerTracker;
    // Start is called before the first frame update
    void Start()
    {
        allPlacements = GameController.GetPlacementObjects();
        
    }

    // Update is called once per frame
    void Update()
    {
        vertical = Mathf.RoundToInt(Input.GetAxisRaw("Vertical" + playerTracker.playerNum.ToString()));
        horizontal = Mathf.RoundToInt(Input.GetAxisRaw("Horizontal" + playerTracker.playerNum.ToString()));


        if (Input.GetButtonDown("Jump" + playerTracker.playerNum.ToString()))
        {
            Action();
        } 
    }

    private void Action()
    {
        if (selectedPlacement.m_tower == null)
        {
            selectedPlacement.BuildTower();
        }

    }

    void MoveToClosestPlace()
    {
        
    }
}
