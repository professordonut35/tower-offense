﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempTower : MonoBehaviour
{

    public GameObject PelletPrefab;
    public AudioClip fireSFX1;
    public AudioClip fireSFX2;
    public AudioClip fireSFX3;
    float coolDown;
    public float coolDownTime;
    PlayerTracker pt;

    private void Start()
    {
        pt = transform.parent.GetComponent<PlayerTracker>();
    }

    void Update()
    {

        if (coolDown > 0)
        {
            coolDown -= Time.deltaTime;            
        }

        //if (Input.GetAxis("Jump" + pt.playerNum) > 0)
        //{
        //    SpawnPellet();
        //}
        Vector2 lookPointInput = new Vector2(Input.GetAxis("Horizontal" + pt.playerNum), Input.GetAxis("Vertical" + pt.playerNum));
        //Vector2 lookPointInput = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 lookPoint = transform.position + new Vector3(lookPointInput.x,lookPointInput.y,0);
        lookPoint.z = this.transform.position.z;
        
        if (lookPointInput.magnitude<.5)
        {
            this.transform.rotation = this.transform.rotation;
        }
        else
        {
            this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, Quaternion.LookRotation(Vector3.forward, lookPoint - this.transform.position), 50f);
        }

        //GetComponent<ShipMotor>().HandleMovementInput(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));
    }

    /// <summary>
    /// Creates a homing missile based on the prefab stored in HomingMissilePrefab at the position of the player facing the same direction.
    /// </summary>
    public void SpawnPellet()
    {
        if (coolDown <= 0)
        {
            Instantiate<GameObject>(PelletPrefab, this.transform.position, this.transform.rotation);
            coolDown = coolDownTime;
            SoundManager.instance.RandomizeSfx(fireSFX1, fireSFX2, fireSFX3);
        }        
    }
}
