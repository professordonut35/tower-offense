﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


public class ScoreBar : MonoBehaviour
{

    public int reversed;

    public float score;

    public float winScore;

    public float maxSize;

    public float y;

    public GameObject winScreen;
    public AudioClip winSFX;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3( Mathf.Clamp((score / winScore) * maxSize, 0.1f, maxSize) * reversed ,y,1);

        if(score == winScore)
        {
            SoundManager.instance.musicSource.clip = winSFX;
            GameObject ws = Instantiate(winScreen);
            ws.transform.position = Vector3.zero;

            if (reversed < 0)
            {
                ws.transform.GetChild(1).GetComponent<TextMeshPro>().text = "BLUE WINS!";
            }
            else
            {
                ws.transform.GetChild(1).GetComponent<TextMeshPro>().text = "RED WINS!";
            }

            Invoke("DelaySceneChange", 3f);


            
        }
    }

    public void DelaySceneChange()
    {
        SceneManager.LoadScene(0);
    }
}
