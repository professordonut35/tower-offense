﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class McGuffin : MonoBehaviour
{
    GameObject player;
    public bool wasCaught;
    public float personalDistance;
    public float followSharpness;
    Vector3 _followOffset;
    Vector3 startPosition;
    public AudioClip magicSFX;

    void Start()
    {
        startPosition = transform.position;
    }

    void LateUpdate()
    {
        if (player != null)
        {
            // Apply that offset to get a target position.
            Vector3 targetPosition = player.transform.position + _followOffset;

            //Make offset
            _followOffset = (transform.position - player.transform.position).normalized * personalDistance;

            // Keep our z position unchanged.
            targetPosition.z = transform.position.z;

            // Smooth follow.    
            transform.position += (targetPosition - transform.position) * followSharpness;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name == "Player")
        {
            player = collision.gameObject;
            //Make offset
            //SoundManager.instance.PlaySingle(magicSFX);
            GetComponent<AudioSource>().Play();
            
            _followOffset = (transform.position - player.transform.position).normalized * personalDistance;
            wasCaught = true;
        }
    }

    public void Drop()
    {
        player = null;
        wasCaught = false;
    }

    public void ResetPosition()
    {
        transform.position = startPosition;
        player = null;
        wasCaught = false;
    }

}
