﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeZone : MonoBehaviour
{

    public Vector3 textOffset;
    //public HomeZone other;
    [SerializeField]
    private bool lastTouched;


    public Color colorStart = Color.red;
    public Color colorEnd = Color.white;

    [SerializeField] PlayerTracker pt;

    public SpriteRenderer sr;

    public AnimationCurve colorFlashCurve;
    public float flashLength;
    float flashLengthLeft;

    GameController gm;
    PlayerMovement pm;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        gm = FindObjectOfType<GameController>();
        //score.transform.position = Camera.main.WorldToScreenPoint(transform.position + textOffset);
    }

    // Update is called once per frame
    void Update()
    {
        if (flashLengthLeft > 0)
        {
            flashLengthLeft -= Time.deltaTime;
        }
        if (!lastTouched) { sr.color = Color.Lerp(colorEnd, colorStart, colorFlashCurve.Evaluate(flashLengthLeft / flashLength)); }
        else { sr.color = Color.white; }
        
        //print(flashLengthLeft / flashLength);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.GetComponent<McGuffin>())
        {        
                //Tell the gamecontroller that the goal was hit and what player is this one for
                gm.GoalReached();
        }
    }

    
}


