﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    float timer;
    public float timerTime;
    SpriteRenderer sr;

    int lastTouch = 0;

    public Color[] startColour;
    public Color endColour;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();   
    }

    // Update is called once per frame
    void Update()
    {
        if (lastTouch > 0)
        {
            timer -= Time.deltaTime;

        }

        if (timer < 0)
        {
            Destroy(gameObject);
        }
        if (lastTouch > 0)
        {
        sr.color = Color.Lerp(endColour, startColour[lastTouch-1], (timer/timerTime));

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerMovement>())
        {

            PlayerMovement p = collision.gameObject.GetComponent<PlayerMovement>();
            /*
            if(p.playerNum != lastTouch)
            {
                lastTouch = p.playerNum;

                timer = timerTime;
            }
            */
        }
    }
}