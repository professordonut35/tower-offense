﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

public class GameController : MonoBehaviour
{
    public const float ROTATE_TIME = 0.5f;

    private static Camera sm_sceneCamera;
    private static List<GameObject> Towers;
    private static List<Placement> Placements;
    private static int maxNumTowers;
    public static bool maxNumReached;
    private static PlayerMovement sm_player;


    private static int round;
    public ScoreBar[] score;
    private static McGuffin flag;
    public PlayerTracker pt;
    public GameObject blueHomeZone;
    public GameObject redHomeZone;
    public int numberOfTowersThatCanBeBuilt;
    //private static GameObject sm_playerObject;
    //private static GameObject sm_enemyObject;

    private static GameController sm_gameController;
    private static List<GameObject> bothHomeZones;
    private static List<GameObject> bothScoreBar;

    public AudioClip goalSFX;

    private void Awake()
    {
        if ( null == sm_gameController )
        {
            sm_gameController = this;
        }
        else
        {
            Destroy( this );
        }

        maxNumTowers = numberOfTowersThatCanBeBuilt;
        //GetHomeZoneObjects();
        GetPlayer();
        GetMcGuffin();
        GetPlacementObjects();
        UnlockAllPlacements();
        
    }

    public static void LockAllPlacements()
    {
        for (int i = 0; i < Placements.Count; i++)
        {
            Placement thisPlace = Placements[i].GetComponent<Placement>();
            if (thisPlace.canBuildHere)
            {
                thisPlace.canBuildHere = false;
            }
        }
    }


    public static void UnlockAllPlacements()
    {
        for (int i = 0; i < Placements.Count; i++)
        {
            Placement thisPlace = Placements[i].GetComponent<Placement>();
            if (!thisPlace.canBuildHere)
            {
                thisPlace.canBuildHere = true;
            }
        }
    }

    public void GoalReached()
    {
        SoundManager.instance.PlaySingle(goalSFX);
        IncreaseScore(sm_player.playerNum);
        flag.ResetPosition();
        ChangePlayers();

        
    }

    public void IncreaseScore(int playerNumber)
    {
        score[playerNumber-1].score++;
          
    }

    public void LostRound()
    {
        
        if (sm_player.playerNum == 2)
        {
            sm_player.transform.position = blueHomeZone.transform.position + new Vector3(-0.7f, 0, 0);
            print("blue");
        }
        else
        {
            sm_player.transform.position = redHomeZone.transform.position + new Vector3(-0.7f,0,0);
            print("red");

        }
        flag.Drop();
        sm_player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        ChangePlayers();

    }


    public void ChangePlayers()
    {
        sm_player.TeamSwitch();


        pt.TeamSwitch();

        iTween.PunchPosition(Camera.main.gameObject, new Vector3(0, 0.6f, 0f), 0.4f);


    }

    public static void LockPlacesWithTower()
    {
        for (int i = 0; i < Placements.Count; i++)
        {
            Placement thisPlace = Placements[i].GetComponent<Placement>();
            if (thisPlace.m_tower != null)
            {
                thisPlace.canBuildHere = false;
            }
        }
    }

    /// <summary>
    /// Returns a list of GameObject with a Tower component found in the scene. If one does not exist, an error is logged
    /// </summary>
    /// <returns>The first player found in the scene.</returns>
    public static List<GameObject> GetTowerObjects()
    {
        GetObjectsOfType<TempTower>(out Towers);
        if(Towers.Count >= maxNumTowers-1)
        {
            maxNumReached = true;
        }
        return Towers;
    }


    /// <summary>
    /// Returns a list of GameObject with a Placement component found in the scene. If one does not exist, an error is logged
    /// </summary>
    /// <returns>The first player found in the scene.</returns>
    public static List<Placement> GetPlacementObjects()
    {
        List<GameObject> placementGameObjects;
        GetObjectsOfType<Placement>(out placementGameObjects);

        for (int i = 0; i < placementGameObjects.Count; i++)
        {
            Placements.Add(placementGameObjects[i].GetComponent<Placement>());
        }
        return Placements;
    }

    public static Camera GetCamera()
    {
        if ( null == sm_sceneCamera )
        {
            GetComponentOfType<Camera>( out sm_sceneCamera );
        }
        return sm_sceneCamera;
    }

    public static McGuffin GetMcGuffin()
    {
        if (null == flag)
        {
            GetComponentOfType<McGuffin>(out flag);
        }
        return flag;
    }

    public static PlayerMovement GetPlayer()
    {
        if (null == sm_player)
        {
            GetComponentOfType<PlayerMovement>(out sm_player);
        }
        return sm_player;
    }

    /// <summary>
    /// Returns the first GameObject with a component found in the scene. If one does not exist, an error is logged
    /// </summary>
    /// <returns>The first enemy found in the scene.</returns>
    private static GameObject GetObjectOfType<T>( out GameObject foundObject ) where T : Component
    {
        foundObject = null;
        GameObject[] rootObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        foreach ( GameObject rootObject in rootObjects )
        {
            T searchResult = rootObject.GetComponentInChildren<T>();
            if ( null != searchResult )
            {
                foundObject = searchResult.gameObject;
                break;
            }
        }
        if ( null == foundObject )
        {
            Debug.LogError( string.Format( "There are no objects of type {0} in the scene.", typeof( T ).ToString() ) );
        }
        
        return foundObject;
    }

    /// Returns a list of GameObject with an component found in the scene. If one does not exist, an error is logged
    /// </summary>
    /// <returns>The first enemy found in the scene.</returns>
    private static List<GameObject> GetObjectsOfType<T>(out List<GameObject> foundObjects) where T : Component
    {
        foundObjects = null;
        GameObject[] rootObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (GameObject rootObject in rootObjects)
        {
            List<T> searchResult = GameObject.FindObjectsOfType<T>().ToList();
            if (null != searchResult)
            {
                foundObjects = new List<GameObject>();
                for (int i = 0; i < searchResult.Count; i++)
                {
                    foundObjects.Add(searchResult[i].GetComponent<GameObject>());
                }
                break;
            }
        }
        if (null == foundObjects)
        {
            Debug.LogError(string.Format("There are no objects of type {0} in the scene.", typeof(T).ToString()));
        }
        Debug.Log(string.Format("There are " + foundObjects.Count + " objects of type {0} in the scene.", typeof(T).ToString()));
        return foundObjects;
    }

    /// <summary>
    /// Returns the first GameObject with an Enemy component found in the scene. If one does not exist, an error is logged
    /// </summary>
    /// <returns>The first enemy found in the scene.</returns>
    private static T GetComponentOfType<T>( out T foundObject ) where T : Component
    {
        foundObject = null;
        GameObject[] rootObjects = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
        foreach ( GameObject rootObject in rootObjects )
        {
            T searchResult = rootObject.GetComponentInChildren<T>();
            if ( null != searchResult )
            {
                foundObject = searchResult;
                break;
            }
        }
        if ( null == foundObject )
        {
            Debug.LogError( string.Format( "There are no objects of type {0} in the scene.", typeof( T ).ToString() ) );
        }
        return foundObject;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            RestartGame();
        }
    }

    private void RestartGame()
    {
        if (Towers == null)
        {
            return;
        }
        UnlockAllPlacements();

        LockPlacesWithTower();

        for (int i = 0; i < Towers.Count; i++)
        {
            Destroy(Towers[i].gameObject);
        }
        Towers = null;
        maxNumReached = false;

    }
    /// <summary>
    /// Move the first enemy found to a new random position on screen
    /// </summary>
    //public static void MoveEnemy()
    //{
    //    if ( null == sm_sceneCamera )
    //    {
    //        GameObject cameraObject;
    //        GetObjectOfType<Camera>( out cameraObject );
    //        if ( null != cameraObject )
    //        {
    //            sm_sceneCamera = cameraObject.GetComponent<Camera>();
    //        }
    //    }
    //    GameObject enemyObject = GetEnemyObject();
    //    if ( null != enemyObject )
    //    {
    //        enemyObject.transform.position = Vector3.up * UnityEngine.Random.Range( -sm_sceneCamera.orthographicSize, sm_sceneCamera.orthographicSize ) + Vector3.right * UnityEngine.Random.Range( -sm_sceneCamera.orthographicSize * sm_sceneCamera.aspect, sm_sceneCamera.orthographicSize * sm_sceneCamera.aspect );
    //    }
    //    else
    //    {
    //        Debug.Log( "Couldn't find an enemy to move." );
    //    }
    //}

    //public static void RotateScreenClockwise90Deg()
    //{
    //    Camera camera = GetCamera();
    //    float angleRemainder = camera.transform.eulerAngles.z % 90;
    //    if ( angleRemainder != 0 )
    //    {
    //        sm_gameController.StopAllCoroutines();
    //        camera.transform.rotation = Quaternion.Euler( 0, 0, camera.transform.eulerAngles.z + ( 90 - angleRemainder ) );
    //    }
    //    sm_gameController.StartCoroutine( RotateCamera( camera ) );
    //}

    //private static IEnumerator RotateCamera( Camera camera )
    //{
    //    float startRotation = camera.transform.eulerAngles.z;
    //    float timer = 0;
    //    while ( timer < ROTATE_TIME )
    //    {
    //        timer = Mathf.Clamp( timer + Time.deltaTime, 0, ROTATE_TIME );
    //        camera.transform.rotation = Quaternion.Euler( 0, 0, startRotation + 90 * ( -Mathf.Pow( 2, -10 * timer / ROTATE_TIME ) + 1 ) );
    //        yield return null;
    //    }
    //}

}
