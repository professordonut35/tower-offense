﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    CapsuleCollider2D cc;
    float startTime;
    public float coolDown;
    bool exitWall;
    public AudioClip hitSFX1;
    public AudioClip hitSFX2;
    public AudioClip hitSFX3;
    public AudioClip hitSFX4;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("hit");
        if(collision.gameObject.layer == 14)
        {
            SoundManager.instance.RandomizeSfx(hitSFX1, hitSFX2, hitSFX3, hitSFX4);

                Destroy(gameObject);
        
            
        }
        if(collision.gameObject.layer == 12)
        {
            Destroy(this.gameObject);
        }
        if(collision.gameObject.name == "Player")
        {
            collision.gameObject.GetComponent<PlayerMovement>().GetHit();
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name == "Home Zone")
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            Debug.Log("exit the layer");
            gameObject.layer = LayerMask.NameToLayer("PelletLayer");
        }
    }

    private void Start()
    {
        cc = GetComponent<CapsuleCollider2D>();
        //cc.enabled = false;
        //startTime = Time.time;
        gameObject.layer = LayerMask.NameToLayer("IgnoreWall");

    }

    public float ForwardSpeed = 1;

    // In Update, you should rotate and move the missile to rotate it towards the player.  It should move forward with ForwardSpeed and rotate at RotateSpeedInDeg.
    // Do not use the RotateTowards or LookAt methods.
    void Update()
    {
        if (exitWall)
        {
            //cc.enabled = true;
        }
        transform.Translate(0, Time.deltaTime * ForwardSpeed, 0);

        Debug.DrawLine(transform.position, transform.position + transform.up, Color.green);
    }
}
