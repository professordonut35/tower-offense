﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    
    public float maxSpeed;
    public float accelRate;
    float horizontal;
    float vertical;
    Vector3 movement;
    [SerializeField]
    [Range(1,2)]
    public int playerNum;
    [SerializeField]
    float dashSpeed;
    public float dashCooldown;
    float dashCooldownTimeLeft;
    float dashDown;
    public Vector2 dashDir;
    public float dashTime;
    float dashTimeLeft;
    SpriteRenderer sr;
    GameController gm;

    public AudioClip dashSFX;
    public AudioClip deathSFX;

    float bugCooldown = 0.18f;
    float bugTimeLeft;

    public void GetHit()
    {

        if (bugTimeLeft <= 0)
        {
            SoundManager.instance.PlaySingle(deathSFX);
            Debug.Log("player hit");
            gm.LostRound();
            bugTimeLeft = bugCooldown;
        }
        else { print(bugTimeLeft); }
        
    }

    public Color[] colorStart;
    public Color colorEnd;
    public AnimationCurve dashIndicatorCurve;
    Rigidbody2D rb;
    TrailRenderer tr;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        tr = GetComponent<TrailRenderer>();
        gm = FindObjectOfType<GameController>();
        StartCoroutine(DynamicMovement());
    }
    
    void Update()
    {
        vertical = Input.GetAxisRaw("Vertical" + playerNum.ToString());
        horizontal = Input.GetAxisRaw("Horizontal" + playerNum.ToString());
        dashDown = Input.GetAxis("Jump" + playerNum.ToString());

        //dashDown = Input.GetButton("Jump" + playerNum.ToString());

        movement = new Vector3(horizontal, vertical, 0);

        movement.Normalize();

        movement *= accelRate;

        sr.color = Color.Lerp(colorStart[playerNum-1],colorEnd, dashIndicatorCurve.Evaluate(dashCooldownTimeLeft/dashCooldown));
        tr.startColor = colorStart[playerNum - 1];

        bugTimeLeft -= Time.deltaTime;
    }

    IEnumerator DynamicMovement()
    {
        yield return new WaitForFixedUpdate();

        if (Mathf.Abs(rb.velocity.x) < maxSpeed) { horizontal = 0; }
        if (Mathf.Abs(rb.velocity.x) < maxSpeed) { vertical = 0; }

        rb.velocity = new Vector2(rb.velocity.x + movement.x, rb.velocity.y + movement.y);

        if (dashCooldownTimeLeft < 0 )
        {
            if (dashDown > 0)
            {
                dashTimeLeft = dashTime;
                StartCoroutine(Dash());
                sr.color = colorStart[playerNum-1];
                dashDir = movement.normalized;
            }
            else
            {
                StartCoroutine(DynamicMovement());
            }
        }
        else
        {
            
            dashCooldownTimeLeft -= Time.deltaTime;
            StartCoroutine(DynamicMovement());
        }
    }

    IEnumerator Dash()
    {
        yield return new WaitForFixedUpdate();

        //speed maintenance
        SoundManager.instance.PlaySingle(dashSFX);
        rb.velocity = dashDir * dashSpeed;

        dashTimeLeft -= Time.deltaTime;

        //dash timer
        if(dashTimeLeft < 0)
        {
            dashCooldownTimeLeft = dashCooldown;
            dashDir = Vector2.zero;
            StartCoroutine(DynamicMovement());            
        }
        else
        {
            StartCoroutine(Dash());
        }
    }

    public void TeamSwitch()
    {

        if (playerNum == 1)
        {
            playerNum = 2;
        }
        else
        {
            playerNum = 1;
        }
    }
}