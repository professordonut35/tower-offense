﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : MonoBehaviour
{

    [Range(1,2)]
    public int playerNum;


    public void TeamSwitch()
    {

        if (playerNum == 1)
        {
            playerNum = 2;
        }
        else
        {
            playerNum = 1;
        }
    }
}
