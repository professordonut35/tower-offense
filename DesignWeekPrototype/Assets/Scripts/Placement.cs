﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placement : MonoBehaviour
{
    public GameObject towerPrefab;
    public GameObject m_tower;
    public bool canBuildHere;

    public enum Location
    {
        Top,
        Bottom,
        Left,
        Right
    }

    public Location buttonLocation;

    void Start()
    {
        m_tower = null;
        canBuildHere = true;
    }


    private void Update()
    {
        if (Input.GetButtonDown("Fire"+buttonLocation.ToString()+transform.parent.GetComponent<PlayerTracker>().playerNum.ToString()))
        {
            BuildTower();
        }
    }

    public void BuildTower()
    {
        //If there is no tower, and you can build one here, do so.
        if (m_tower == null && canBuildHere)
        {
            Debug.Log("Tower Spawned", this.gameObject);
            m_tower = Instantiate(towerPrefab, transform.position, Quaternion.identity, transform.parent);
            GameController.GetTowerObjects();
        }

        if (!canBuildHere)
        {
            Debug.Log("Tower was built here last round. Build somewhere else.");
        }

        if (m_tower != null)
        {
            m_tower.GetComponent<TempTower>().SpawnPellet();
        }
    }

}
